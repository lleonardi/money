<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../config/database.php';
    include_once '../models/monthlyTransactionsByYear.php';

    // Instantiate DB & connect
    $database = new Database();
    $db = $database->connect();
    $year = $_GET['year'];

    // Instantiate transactions object
    $transactions = new MonthlyTransactionsByYear($db);

    // Transactions query
    $result = $transactions->read($year);

    // Get row count
    $num = $result->rowCount();

    // Check if any transaction
    if($num > 0) {
        // Transactions array
        $transactions_arr = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            extract($row);

            $transaction_item = array(
                'month' => $month,
                'monthly_sum' => $monthly_sum
            );

            // Push to "data"
            array_push($transactions_arr, $transaction_item);
        }

    // Turn to JSON & output
    echo json_encode($transactions_arr, JSON_NUMERIC_CHECK);

    } else {
        // No transactions
        echo json_encode(
            array('message' => 'No transactions found')
        );
    }