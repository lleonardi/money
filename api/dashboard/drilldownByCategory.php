<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../config/database.php';
    include_once '../models/summaryByCategory.php';

    // Instantiate DB & connect
    $database = new Database();
    $db = $database->connect();
    $year = $_GET['year'];

    // Instantiate transactions object
    $categories = new DrilldownByCategory($db);

    // Categories query
    $result = $categories->read($year);
    
    // Get row count
    $num = $result->rowCount();

    // Check if any category
    if($num > 0) {
        // Categories array
        $categories_arr = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            extract($row);

            $category_item = array(
                'category_name' => $category_name,
                'subcategory_name' => $subcategory_name,
                'total' => $total
            );

            // Push to "data"
            array_push($categories_arr, $category_item);
        }

    // Turn to JSON & output
    echo json_encode($categories_arr, JSON_NUMERIC_CHECK);

    } else {
        // No data
        echo json_encode(
        array('message' => 'No data found for '.$year)
        );
    }