<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../config/database.php';
    include_once '../models/topCategories.php';

    // Instantiate DB & connect
    $database = new Database();
    $db = $database->connect();

    // Instantiate transactions object
    $top_categories = new TopCategories($db);

    // Categories query
    $result = $top_categories->read();
    
    // Get row count
    $num = $result->rowCount();

    // Check if any category
    if($num > 0) {
        // Categories array
        $top_categories_arr = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            extract($row);

            $top_categories_item = array(
                'category_name' => $category_name,
                'spending' => $spending
            );

            // Push to "data"
            array_push($top_categories_arr, $top_categories_item);
        }

    // Turn to JSON & output
    echo json_encode($top_categories_arr, JSON_NUMERIC_CHECK);

    } else {
        // No data
        echo json_encode(
         array('message' => 'No data found')
        );
    }