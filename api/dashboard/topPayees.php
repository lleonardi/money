<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../config/database.php';
    include_once '../models/topPayees.php';

    // Instantiate DB & connect
    $database = new Database();
    $db = $database->connect();

    // Instantiate transactions object
    $top_payees = new TopPayees($db);

    // Categories query
    $result = $top_payees->read();
    
    // Get row count
    $num = $result->rowCount();

    // Check if any category
    if($num > 0) {
        // Categories array
        $top_payees_arr = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            extract($row);

            $top_payees_item = array(
                'payee' => $payee,
                'spending' => $spending
            );

            // Push to "data"
            array_push($top_payees_arr, $top_payees_item);
        }

        // Turn to JSON & output
        echo json_encode($top_payees_arr, JSON_NUMERIC_CHECK);

    } else {
        // No data
        echo json_encode(
            array('message' => 'No data found')
        );
    }