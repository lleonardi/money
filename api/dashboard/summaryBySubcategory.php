<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../config/database.php';
    include_once '../models/summaryBySubcategory.php';

    // Instantiate DB & connect
    $database = new Database();
    $db = $database->connect();
    $year = $_GET['year'];

    // Instantiate transactions object
    $subcategories = new SummaryBySubcategory($db);

    // Categories query
    $result = $subcategories->read($year);
    
    // Get row count
    $num = $result->rowCount();

    // Check if any category
    if($num > 0) {
        // Categories array
        $subcategories_arr = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            extract($row);

            $subcategory_item = array(
                'subcategory_name' => $subcategory_name,
                'subcategory_total' => $subcategory_total
            );

            // Push to "data"
            array_push($subcategories_arr, $subcategory_item);
        }

    // Turn to JSON & output
    echo json_encode($subcategories_arr, JSON_NUMERIC_CHECK);

    } else {
        // No data
        echo json_encode(
        array('message' => 'No data found for '.$year)
        );
    }