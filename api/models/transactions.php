<?php 
    class Transactions {
        // DB stuff
        private $conn;
        private $accounts = 'accounts';
        private $categories = 'categories';
        private $subcategories = 'subcategories';
        private $transactions = 'transactions';
        private $users = 'users';

        // Transactions Properties
        public $id;
        public $transaction_date;
        public $payee;
        public $payer;
        public $category;
        public $credit;
        public $debit;
        public $category_id;
        public $category_name;
        public $subcategory_name;

        // Constructor with DB
        public function __construct($db) {
            $this->conn = $db;
        }

        // Get Posts
        public function read() {
            $query = "SELECT a.id as account, c.name as category_name, u.name as payer, s.name as subcategory_name, t.id, t.transaction_date, t.payee, t.credit, t.debit 
                        FROM " . $this->transactions . " t 
                        LEFT JOIN " . $this->categories . " c ON t.category = c.id 
                        LEFT JOIN " . $this->subcategories . " s ON t.subcategory = s.id 
                        LEFT JOIN " . $this->users . " u ON t.payer = u.id 
                        LEFT JOIN " . $this->accounts . " a ON t.account = a.id 
                        ORDER BY t.transaction_date ASC";

            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // Execute query
            $stmt->execute();

            return $stmt;
        }
    }
