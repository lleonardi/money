<?php 
    class SummaryBySubcategory {
        // DB stuff
        private $conn;
        private $table = 'transactions';

        // Transactions Properties
        public $subcategory_name;
        public $subcategory_total;

        // Constructor with DB
        public function __construct($db) {
            $this->conn = $db;
        }

        // Get Posts
        public function read($year) {
            // Create query

            if ($year) {
                $query = "SELECT c.name as subcategory_name, SUM(t.debit) AS subcategory_total 
                        FROM " . $this->table . " t
                        LEFT JOIN subcategories c ON t.subcategory = c.id  
                        WHERE t.transaction_date BETWEEN '" . $year . "-01-01' AND '" . $year . "-12-31'
                        GROUP BY subcategory_name";
            } else {
                $query = "SELECT c.name as subcategory_name, SUM(t.debit) AS subcategory_total 
                        FROM " . $this->table . " t
                        LEFT JOIN subcategories c ON t.subcategory = c.id  
                        GROUP BY subcategory_name";
            }

            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // Execute query
            $stmt->execute();

            return $stmt;
        }
    }
