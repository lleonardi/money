<?php 
    class AccountsInfo {
        // DB stuff
        private $conn;
        private $table_accounts = 'accounts';
        private $table_account_type = 'account_type';
        private $table_users = 'users';

        // Transactions Properties
        public $name;

        // Constructor with DB
        public function __construct($db) {
            $this->conn = $db;
        }

        // Get Categoreis
        public function read() {
            // Create query

            $query = "SELECT a.id, a.name, t.type as account_type, a.number, u.name as owner_name, u.surname as owner_surname 
            FROM " . $this->table_accounts . " a 
            LEFT JOIN " . $this->table_account_type . " t ON a.type = t.id
            LEFT JOIN " . $this->table_users . " u ON a.owner = u.id";

            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // Execute query
            $stmt->execute();

            return $stmt;
        }
    }
