<?php 
    class MonthlyTransactionsByYear {
        // DB stuff
        private $conn;
        private $table = 'transactions';

        // Transactions Properties
        public $month;
        public $monthly_sum;

        // Constructor with DB
        public function __construct($db) {
            $this->conn = $db;
        }

        // Get Posts
        public function read($year) {
            // Create query
            $query = "SELECT month(`transaction_date`) AS month, SUM(debit) AS monthly_sum
                        FROM " . $this->table . " t 
                        WHERE t.transaction_date BETWEEN '" . $year . "-01-01' AND '" . $year . "-12-31' 
                        GROUP BY MONTH(`transaction_date`);";

            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // Execute query
            $stmt->execute();

            return $stmt;
        }
    }
