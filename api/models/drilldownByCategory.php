<?php 
    class DrilldownByCategory {
        // DB stuff
        private $conn;
        private $table1 = 'transactions';
        private $table2 = 'categories';
        private $table3 = 'subcategories';

        // Transactions Properties
        public $category_name;
        public $subcategory_name;
        public $total;

        // Constructor with DB
        public function __construct($db) {
            $this->conn = $db;
        }

        // Get Posts
        public function read($year) {
            // Create query

            if ($year) {
                $query = "SELECT c.name AS category_name, s.name AS subcategory_name, SUM(t.debit) AS total 
                    FROM " . $this->table1 . " t
                    LEFT JOIN " . $this->table2 . " c ON t.category = c.id 
                    RIGHT JOIN " . $this->table3 . " s ON t.subcategory = s.id
                    WHERE t.transaction_date BETWEEN '" . $year . "-01-01' AND '" . $year . "-12-31'
                    GROUP BY subcategory
                    ORDER BY category_name";
            } else {
                $query = "SELECT c.name AS category_name, s.name AS subcategory_name, SUM(t.debit) AS total 
                    FROM " . $this->table1 . " t
                    LEFT JOIN " . $this->table2 . " c ON t.category = c.id 
                    RIGHT JOIN " . $this->table3 . " s ON t.subcategory = s.id
                    GROUP BY subcategory
                    ORDER BY category_name";
            }

            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // Execute query
            $stmt->execute();

            return $stmt;
        }
    }
