<?php 
    class TopPayees {
        // DB stuff
        private $conn;
        private $table = 'transactions';

        // Transactions Properties
        public $payee;
        public $spending;

        // Constructor with DB
        public function __construct($db) {
            $this->conn = $db;
        }

        // Get Categoreis
        public function read() {
            // Create query

            $query = "SELECT payee, SUM(debit) AS spending FROM " . $this->table . " GROUP BY payee ORDER BY spending DESC";

            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // Execute query
            $stmt->execute();

            return $stmt;
        }
    }
