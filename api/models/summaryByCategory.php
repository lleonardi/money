<?php 
    class SummaryByCategory {
        // DB stuff
        private $conn;
        private $table1 = 'transactions';
        private $table2 = 'categories';

        // Transactions Properties
        public $category_name;
        public $category_total;

        // Constructor with DB
        public function __construct($db) {
            $this->conn = $db;
        }

        // Get Posts
        public function read($year) {
            // Create query

            if ($year) {
                $query = "SELECT c.name AS category_name, SUM(t.debit) AS category_total 
                        FROM " . $this->table1 . " t
                        LEFT JOIN " . $this->table2 . " c ON t.category = c.id 
                        WHERE t.transaction_date BETWEEN '" . $year . "-01-01' AND '" . $year . "-12-31'
                        AND t.category <> 0
                        AND t.category <> 13
                        GROUP BY category_name";
            } else {
                $query = "SELECT c.name AS category_name, SUM(t.debit) AS category_total 
                        FROM " . $this->table1 . " t
                        LEFT JOIN " . $this->table2 . " c ON t.category = c.id 
                        AND t.category <> 0
                        AND t.category <> 13
                        GROUP BY category_name";
            }

            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // Execute query
            $stmt->execute();

            return $stmt;
        }
    }
