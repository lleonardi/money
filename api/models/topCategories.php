<?php 
    class TopCategories {
        // DB stuff
        private $conn;
        private $table1 = 'transactions';
        private $table2 = 'categories';

        // Transactions Properties
        public $category_name;
        public $spending;

        // Constructor with DB
        public function __construct($db) {
            $this->conn = $db;
        }

        // Get Categoreis
        public function read() {
            // Create query

            $query = "SELECT c.name as category_name, SUM(t.debit) as spending 
                FROM " . $this->table1 . " t
                LEFT JOIN " . $this->table2 . " c ON t.category = c.id
                GROUP BY category_name
                ORDER BY spending desc;";

            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // Execute query
            $stmt->execute();

            return $stmt;
        }
    }
