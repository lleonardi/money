<?php 
    class Subcategories {
        // DB stuff
        private $conn;
        private $table = 'subcategories';

        // Transactions Properties
        public $id;
        public $name;

        // Constructor with DB
        public function __construct($db) {
            $this->conn = $db;
        }

        // Get Categoreis
        public function read() {
            // Create query

            $query = "SELECT id, name FROM " . $this->table . " ORDER BY name ASC";

            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // Execute query
            $stmt->execute();

            return $stmt;
        }
    }
