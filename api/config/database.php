<?php 
    class Database {
        // DB Params
        private $host = 'localhost';
        private $db_name = 'money';
        private $username = 'money_user';
        private $password = '1234';
        private $conn;

        // DB Connect
        public function connect() {
            $this->conn = null;

            try { 
                $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name . ';charset=utf8', $this->username, $this->password);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, 0);
                $this->conn->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, 0);
            } catch(PDOException $e) {
                echo 'Connection Error: ' . $e->getMessage();
            }

            return $this->conn;
        }
  }