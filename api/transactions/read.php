<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../config/database.php';
    include_once '../models/transactions.php';

    // Instantiate DB & connect
    $database = new Database();
    $db = $database->connect();

    // Instantiate transactions object
    $transactions = new Transactions($db);

    // Transactions query
    $result = $transactions->read();

    // Get row count
    $num = $result->rowCount();

    // Check if any transaction
    if($num > 0) {
        // Transactions array
        $transactions_arr = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            extract($row);

            $transaction_item = array(
                'account' => $account,
                'date' => $transaction_date,
                'payee' => $payee,
                'payer' => $payer,
                'credit' => $credit,
                'debit' => $debit,
                'category_name' => $category_name,
                'subcategory_name' => $subcategory_name
            );

            // Push to "data"
            array_push($transactions_arr, $transaction_item);
        }

    // Turn to JSON & output
    echo json_encode($transactions_arr, JSON_NUMERIC_CHECK);

    } else {
        // No transactions
        echo json_encode(
        array('message' => 'No transactions found')
        );
    }