<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../config/database.php';
    include_once '../models/accountsInfo.php';

    // Instantiate DB & connect
    $database = new Database();
    $db = $database->connect();

    // Instantiate transactions object
    $accounts = new AccountsInfo($db);

    // Transactions query
    $result = $accounts->read();

    // Get row count
    $num = $result->rowCount();

    if($num > 0) {
        $accounts_arr = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            extract($row);

            $account_item = array(
                'id' => $id,
                'name' => $name,
                'account_type' => $account_type,
                'number' => $number,
                'owner_name' => $owner_name,
                'owner_surname' => $owner_surname
            );

            // Push to "data"
            array_push($accounts_arr, $account_item);
        }

    // Turn to JSON & output
    echo json_encode($accounts_arr, JSON_NUMERIC_CHECK);

    } else {
        echo json_encode(
        array('message' => 'No account infos found')
        );
    }