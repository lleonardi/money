<?php 
    // Headers
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include_once '../config/database.php';
    include_once '../models/categories.php';

    // Instantiate DB & connect
    $database = new Database();
    $db = $database->connect();

    // Instantiate transactions object
    $categories = new Categories($db);

    // Categories query
    $result = $categories->read();
    
    // Get row count
    $num = $result->rowCount();

    // Check if any category
    if($num > 0) {
        // Categories array
        $categories_arr = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            extract($row);

            $category_item = array(
                'id' => $id,
                'name' => $name
            );

            // Push to "data"
            array_push($categories_arr, $category_item);
        }

    // Turn to JSON & output
    echo json_encode($categories_arr, JSON_NUMERIC_CHECK);

    } else {
        // No data
        echo json_encode(
        array('message' => 'No data found')
        );
    }