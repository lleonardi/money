# Money

## Introduction

This app is intended to provide an insight into a your own financial status. It was borne out of the discomfort of giving third parties access to personal financial data. 

The application is provided as Open Source: you are free to download it, use it, modify it. As the author I take no responsibility for any damage caused, whether implicit or not. 

## Prerequisites 

This is a web app based on Angular and requires access to data stored on an API server. It is assumed you have some knowledge of Angular in case you want to modify the code.

Development was carried out on a LAMP stack so to help you get started you can use the MySQL dump in the /api folder with a sample structure. 

The quality of data is key to achieve meaningful information, so do invest as much effort as possible in cleaning the original records and check their accuracy.

## Installation 

### Requirements 

An API server (local or remote, your choice)
A web server, which can be either the API server or the Angular local host dev environment of course
As much data as possible, esp. bank and/or credit card statements

### Installation process

## API server

A code sample can be found in the /php folder, together with a MySQL dump of the structure to help you to get started. You will need to amend the config file accordingly.

## Application 

run `npm install` from the root to set up the app
amend the environment variables as needed to point to your API server
run `ng build` and copy the files from the /dist/Money folder on your server

## Demo

A demo based on fake data (I wish I was making that money!) can be viewed at http://money.camits.co.uk

## Support

Please log issues and defects on the repository but do not expect a quick reply as this is a pet project, not a commercial application. 

## Discharged options

charts.js - pretty and quick to get it set up and running, but lacking control and limited customisations
ag-grid - nearly ideal, but grouping only possible with the Enterprise version (therefore removed due to licensing)
jqwidgets - fairly quick to get set up, but slow when loading large datasets without paging, bugs with Bootstrap (at time of writing) and grouping oddily avilable on grid pages, not whole dataset

## Credits

https://krazydad.com/tutorials/makecolors.php for the random colours generator
https://gist.github.com/niepi/630660 - PHP parser for QIF/OFX

## Hints on importing CSV files

### Date format

The database requires data in the format 'yyyy-mm-dd'. If your CSV file uses a different one, convert the column with the =TEXT(date,format) formula, like this: `=TEXT(A1,"yyyy-mm-dd")`

### Special characters

To avoid fail on import, it may be useful to convert the following characters:

- currency codes (£, $, €, etc.)
- special characters (*, %, ", etc.)

### SQL commands

- Trim strings (remove spaces): `UPDATE money.transactions SET payee = TRIM(payee)`