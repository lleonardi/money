import { Component, OnInit } from '@angular/core';
import { StatementService } from 'src/app/services/statement.service';

@Component({
    selector: 'app-summary',
    templateUrl: './summary.component.html',
    styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

    public total = 0 as number;

    constructor(
        private statementService: StatementService
    ) { }

    ngOnInit(): void {
        this.statementDetails();
    }

    statementDetails(): void {
        this.statementService.getStatement()
            .subscribe( data => {
                data.transactions.forEach(element => {
                    let cred = 0;
                    element.credit === null ? cred = 0 : cred = parseFloat(element.credit);
                    this.total = this.total + cred + element.debit;
                });
            });
    }
}
