import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatementComponent } from './statement.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

fdescribe('StatementComponent', () => {
    let component: StatementComponent;
    let fixture: ComponentFixture<StatementComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ HttpClientModule, FormsModule, ReactiveFormsModule ],
            declarations: [ StatementComponent ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    xit('should return data filtered by account string/name', () => {
        const accounts = ['string1', 'string2', 'string3'];
        const filter = 'string1';
    });
});
